const moment = require('moment')
const express = require('express')
const bodyParser = require('body-parser')
const app = express()

const http = require('http').createServer(app);
const io = require('socket.io')(http);


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/test', function (req, res) {
  res.send('wow')
});

io.on('connection', function (socket) {
  socket.on('chat message', function (msg) {
    io.emit('chat message', msg);
  });
});

const x = () => {
  const dt = moment().format('x')
  console.log(dt)
  io.emit('chat message', dt);
  setTimeout(x, 2000)
}

let z = 0

const y = () => {
  console.log(z++)
  io.emit('test', z);
  setTimeout(y, 1000)
}

http.listen(3333, function () {
  console.log('listening on *:3333');

  x()
  y()

});